import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceApiProvider } from '../../providers/service-api/service-api';
import User from '../../models/user.model';
import { NgForm } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-connexion',
  templateUrl: 'connexion.html',
})
export class ConnexionPage {
  users: Array<User>;
  user: User;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public api: ServiceApiProvider
  ) {

    this.users = api.getUsers();
    this.user = this.navParams.get('user');

  }

   go_user(form: NgForm) {
    let user: User = new User(form.value.email, form.value.password);
    this.navCtrl.push(ConnexionPage, { user: user });

    
    var users = this.users;
    var i = 0;
    var session ='';
    var connected : boolean ;
    for (i = 0; i < users.length; i++) {

      var verif_email = users[i].email;
      var verif_password = users[i].password;

      if (user.email === verif_email && user.password === verif_password) {
       session = 'true';
       
      } else {
        session = 'false';
       
      }
    }
    if(session !='false'){
      connected = true;
    }else{
      connected = false;
    }
    console.log(connected);
   
  }
  
}
