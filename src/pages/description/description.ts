import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CameraProvider } from "../../providers/camera/camera";
import { Screenshot } from '@ionic-native/screenshot';
import { Vibration } from '@ionic-native/vibration';
import { ServiceApiProvider } from "../../providers/service-api/service-api";

@IonicPage()
@Component({
  selector: 'page-description',
  templateUrl: 'description.html',
})
export class DescriptionPage {
  movie: any = null;
  FavMsg: string;
  screen: any;
  state: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public camera: CameraProvider,
    public screenshot: Screenshot,
    private vibration: Vibration,
    public api:ServiceApiProvider)
  {
    this.movie = navParams.get('movie');
    this.movie.isInFavorite = this.api.isMovieInFavorites(this.movie);
  }


  addList() {
    this.vibration.vibrate(1000);
  }

  reset() {
    let self = this;
    setTimeout(() => {
      self.state = false;
    }, 1000);
  }

  screenShot() {
    this.screenshot.save('jpg', 80, this.movie.title + '.jpg').then(res => {
      this.screen = res.filePath;
      this.state = true;
      this.reset();
      alert(this.movie.title + ' à bien était enregistré !');
    });
  }

  takeScreen() {
    this.camera.TakeScreen(this.movie.title);
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad DescriptionPage');
  }

  onAddOrRemoveFavorite() {
    this.addList();
    if (this.movie.isInFavorite) {
      this.api.deleteFavoriteMovie(this.movie);
      this.movie.isInFavorite = false;
      this.FavMsg = "Le film a été supprimé des favoris.";
    }
    else {
      this.api.addFavoriteMovie(this.movie);
      this.movie.isInFavorite = true;
      this.FavMsg = "Le film a été ajouté aux favoris.";
    }

  }

}
