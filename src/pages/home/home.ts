import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ServiceApiProvider } from "../../providers/service-api/service-api";
import { DescriptionPage } from '../description/description';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  loading: boolean = false;
  movies: any[] = null;
  favMovies: any []= null;

  search: any[] = [];
  searchValue: string = '';
  searchPassword: string = '';

  constructor(public navCtrl: NavController, public serviceApi: ServiceApiProvider) {
  }

  onOpenMovie(movie) {
    this.navCtrl.push(DescriptionPage, {
      movie: movie
    });
  }

  ionViewDidLoad() {
        
    this.favMovies = this.serviceApi.getFavoriteMovies();
    this.loading = true;
    setTimeout(() => {
      this.serviceApi.discover().subscribe((data: any) => {
        this.movies = data;
        this.loading = false;
        // console.log(this.movies); // Movie list
      })
    }, 500);
  }

  gohome(){
    this.navCtrl.push(HomePage);
  }
  
  onSearch(query: string) {
    //console.log(query);
    this.serviceApi.search(query).subscribe((data: any) => {
      this.movies = data;
      //console.log(this.search);
    })
    if (query.length == 0) {
      this.gohome();
    }
  }
}
