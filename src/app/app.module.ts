import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { ConnexionPage} from '../pages/connexion/connexion';
import { Camera } from '@ionic-native/camera';
import { Screenshot } from '@ionic-native/screenshot';
import { Vibration } from '@ionic-native/vibration';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ServiceApiProvider } from '../providers/service-api/service-api';
import { DescriptionPage } from '../pages/description/description';
import { CameraProvider } from '../providers/camera/camera';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ConnexionPage,
    DescriptionPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ConnexionPage,
    DescriptionPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ServiceApiProvider,
    Camera,
    CameraProvider,
    Screenshot,
    Vibration
  ]
})
export class AppModule {}
