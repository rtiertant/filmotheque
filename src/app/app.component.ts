import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ServiceApiProvider } from '../providers/service-api/service-api';
import { HomePage } from '../pages/home/home';
import { ConnexionPage } from '../pages/connexion/connexion';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage:any = HomePage;
  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, 
              public statusBar: StatusBar, 
              public splashScreen: SplashScreen, 
              ) {

    

    var connected :boolean ;
    if(connected){
      this.pages = [
        { title: 'Home', component: HomePage },
        { title: '{{user.email}}', component: HomePage},
        { title: 'Log out', component: ConnexionPage }
      ];
    } 
    else {
      this.pages = [
        { title: 'Home', component: HomePage },
        { title: 'Connexion', component: ConnexionPage }
      ];
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

}

