import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import User from '../../models/user.model';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

/*
  Generated class for the ServiceApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServiceApiProvider {
  API_KEY = "779fb1d9551a336896e65bedccc23390";
  LANG = "fr";
  favoriteMovies: any[] = [];

  private users: Array<User>;

  constructor(public http: Http) {

    this.users = [
      new User('adrien.vossough@semifir.com', '0000'),
      new User('roland.laures@semifir.com', '0000'),
      new User('ab@gmail.com', '0000'),
    ];
  }

  getUsers(): Array<User> {
    return this.users;
  }

  //affiche la liste des films
  discover() {
    let BASE_URL = `https://api.themoviedb.org/3/discover/movie?api_key=${this.API_KEY}&language=${this.LANG}`;

    return this.http.get(BASE_URL)
      .map(response => response.json())
      .map(response => response.results);
  }


search(query: string) {
  let BASE_URL = `https://api.themoviedb.org/3/search/movie?api_key=${this.API_KEY}&query=${query}&language=${this.LANG}`;
 
  return this.http.get(BASE_URL)
      .map(response => response.json())
      .map(response => response.results);
}

  getFavoriteMovies() {
    return this.favoriteMovies;
  }

  addFavoriteMovie(movie: any) {
    var exists = false;
    // Check if movie exists
    this.favoriteMovies.forEach((m: any) => {
      if (m.id == movie.id)
        exists = true;
    })
    if (!exists)
      this.favoriteMovies.push(movie);
      console.log(this.favoriteMovies);
  }

  deleteFavoriteMovie(movie: any) {
    var movieIndex : any = '';
    movieIndex = this.favoriteMovies.indexOf(movie);
    this.favoriteMovies.splice(movieIndex,1);
    console.log(this.favoriteMovies);
  }

  isMovieInFavorites(movie: any) {
    var exists = false;
    // Check if movie exists
    this.favoriteMovies.forEach((m: any) => {
        if (m.id == movie.id)
            exists = true;
    })
    return exists;
}



}
