import { Injectable } from '@angular/core';
import { Camera } from '@ionic-native/camera';
import { Screenshot } from '@ionic-native/screenshot';

@Injectable()
export class CameraProvider extends Camera {
  screen: any;
  state: boolean = false;
  movieName: string;

  constructor(public screenshot: Screenshot) {
    super()
  }

  reset() {
    let self = this;
    setTimeout(() => {
      self.state = false;
    }, 1000);
  }

  TakeScreen(namemovie){
    this.screenshot.save('jpg', 80, namemovie).then(res => {
      this.screen = res.filePath;
      this.state = true;
      this.reset();
      alert(namemovie+' à bien était enregistré !');      
    });
  }

}
